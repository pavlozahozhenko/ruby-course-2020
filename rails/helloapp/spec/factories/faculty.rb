FactoryBot.define do
  factory :faculty do
    name { ::FFaker::Lorem.word }
  end
end
