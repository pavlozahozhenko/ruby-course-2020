# Ruby Course 2020

Ruby & Rails programming course, NaUKMA 2020

### Announcements ###
* Консультація №2 - 21.04.2020 13:30
* Залік - 22.04.2020 11:40
* Дедлайн контрольного проекта - 22.04.2020

### Матеріали Курсу
* [Lecture screencasts & online lecture videos](https://drive.google.com/drive/folders/1KVASoqg4tOi8jQROuzvZ0m_3zrHBEmmg)
* [2 лекції 8.04.2020: Asset (js, css, images) management in Rails & automated testing](https://web.microsoftstream.com/video/17c56098-54cf-4ec8-a557-8b8e463c7714)
* [Лекція 15.04.2020: Rails production env, scaling, benchmarks](https://web.microsoftstream.com/video/ecbee079-44b1-4f3d-84ef-8297a2539614)
* [Телеграм-канал курсу](http://t.me/ruby_2020)
* [Repository 2019 року (Gitlab)](https://gitlab.com/pavlozahozhenko/ruby-course-2019)
* [Repository 2017 року (BitBucket)](https://bitbucket.org/burius/ruby_course_2017)
* [Repository 2016 року (BitBucket)](https://bitbucket.org/burius/ror_course)

### Контрольний Проект
* [Вимоги до проекта](https://gitlab.com/pavlozahozhenko/ruby-course-2020/tree/master/students#project-requirements-info)

### Useful Links
#### Ruby
* [Official Ruby documentation (core API)](http://ruby-doc.org/core-2.7.0/)
* [Ruby style guide](https://github.com/bbatsov/ruby-style-guide) by bbatsov
* [Rails API documentation](http://api.rubyonrails.org/)

### Legendary Rubyists on the Web
* [matz](https://twitter.com/yukihiro_matz) - Yukihiro Matsumoto, author of Ruby. MINASWAN (Matz is Nice And So We Are Nice)
* [dhh](https://twitter.com/dhh) - David Heinemeier Hansson, initial author of Ruby on Rails
* [tenderlove](https://twitter.com/tenderlove) - Aaron Patterson, Rails core contributor and a very nice guy
